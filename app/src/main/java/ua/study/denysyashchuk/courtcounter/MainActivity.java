package ua.study.denysyashchuk.courtcounter;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int teamAScore = 0;
    int teamBScore = 0;
    private static TextView timerTV;
    private static TextView periodTV;
    private MyTimer myTimer;
    private static NotificationManager nm;
    private static Notification.Builder mBuilder;
    private int action = 0;
    private static Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        displayForTeamA(teamAScore);
        displayForTeamB(teamBScore);

        timerTV = (TextView) findViewById(R.id.timer);
        periodTV = (TextView) findViewById(R.id.period);

        myTimer = new MyTimer();

        mContext = getBaseContext();

        nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        ServiceConnection mConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className,
                                           IBinder binder) {
                ((CancelNotifService.CancelBinder) binder).service.startService(new Intent(
                        MainActivity.this, CancelNotifService.class));
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        };
        bindService(new Intent(MainActivity.this,
                        CancelNotifService.class), mConnection,
                Context.BIND_AUTO_CREATE);
    }

    /**
     * Displays score for team A
     * @param score - value for displaying
     */
    public void displayForTeamA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_a_score);
        scoreView.setText(String.valueOf(score));
    }

    /**
     * Increases score of team A by 3
     * @param view
     */
    public void teamAThree(View view) {
        teamAScore += 3;
        displayForTeamA(teamAScore);
    }

    /**
     * Increases score of team A by 2
     * @param view
     */
    public void teamATwo(View view) {
        teamAScore += 2;
        displayForTeamA(teamAScore);
    }

    /**
     * Increases score of team A by 1
     * @param view
     */
    public void teamAOne(View view) {
        teamAScore += 1;
        displayForTeamA(teamAScore);
    }

    /**
     * Displays score for team B
     * @param score - value for displaying
     */
    public void displayForTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_b_score);
        scoreView.setText(String.valueOf(score));
    }

    /**
     * Increases score of team B by 3
     * @param view
     */
    public void teamBThree(View view) {
        teamBScore += 3;
        displayForTeamB(teamBScore);
    }

    /**
     * Increases score of team B by 2
     * @param view
     */
    public void teamBTwo(View view) {
        teamBScore += 2;
        displayForTeamB(teamBScore);
    }

    /**
     * Increases score of team B by 1
     * @param view
     */
    public void teamBOne(View view) {
        teamBScore += 1;
        displayForTeamB(teamBScore);
    }

    /**
     * Resets scores of teams
     * @param view
     */
    public void reset(View view) {
        teamAScore = 0;
        teamBScore = 0;
        displayForTeamA(teamAScore);
        displayForTeamB(teamBScore);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Creates notification
        if (myTimer.isCreated()) {
            Intent mIntent = this.getIntent();
            PendingIntent mPendingIntent = PendingIntent.getActivity(this, 0, mIntent, 0);

            mBuilder = new Notification.Builder(this);

            mBuilder.setAutoCancel(true);
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
            mBuilder.setContentIntent(mPendingIntent);
            mBuilder.setOngoing(true);
            mBuilder.build();

            nm.notify(1, mBuilder.build());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Cancel notification
        nm.cancelAll();
    }

    /**
     * Starts/pausing timer
     * @param view
     */
    public void startGame(View view) {
        Button startButton = (Button) findViewById(R.id.startButton);
        switch (action){
            case 0:
                myTimer.create();
                myTimer.start();
                action = 1;
                startButton.setText("Pause Game");
                break;
            case 1:
                myTimer.pause();
                action = 2;
                startButton.setText("Resume Game");
                break;
            case 2:
                myTimer.resume();
                action = 1;
                startButton.setText("Pause Game");
                break;
        }

    }

    /**
     * SToping timer
     * @param view
     */
    public void stopGame(View view) {
        action = 0;
        myTimer.reset();
        Button startButton = (Button) findViewById(R.id.startButton);
        startButton.setText("Start Game");
        nm.cancelAll();
    }

    public static Notification.Builder getmBuilder() {
        return mBuilder;
    }

    public static NotificationManager getNm() {
        return nm;
    }

    public static Context getmContext() {
        return mContext;
    }

    public static TextView getTimerTV() {
        return timerTV;
    }

    public static TextView getPeriodTV() {
        return periodTV;
    }
}
