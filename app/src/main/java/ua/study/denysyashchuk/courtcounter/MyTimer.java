package ua.study.denysyashchuk.courtcounter;

import android.app.Notification;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.util.Log;

import java.text.DecimalFormat;

public class MyTimer {

    private CountDownTimer timer;
    private static int period = 0;
    private static long timeToSave;

    public MyTimer() {
        Log.wtf("Timer", "create");
    }

    /**
     * Return time (in minutes), depending on period of game:
     * 1-4 periods - 10 minutes;
     * breaks between 1st and 2nd or 3rd and 4th periods - 2 minutes
     * break between 2nd and 3rd period - 15 minutes
     * additional period - 5 minutes
     * break before and between additional periods - 2 minutes;
     *
     * @return duration of the game period in minutes
     */
    private int getTime() {
        switch (period) {
            case 0:
            case 2:
            case 4:
            case 6:
                return 10;
            case 1:
            case 5:
                return 2;
            case 3:
                return 15;
            default:
                return period % 2 == 0 ? 5 : 2;

        }
    }

    /**
     * Stops timer and sets period to initial value
     */
    public void reset() {
        Log.wtf("Timer", "reset");
        timer.cancel();
        period = 0;
        MainActivity.getTimerTV().setText("00:00");
        timer = null;
    }

    public void start() {
        Log.wtf("Timer", "start");
        timer.start();
    }

    /**
     * Initializing timer variable
     */
    public void create() {
        this.timer = creatCountDownTimer();
    }

    /**
     * Creates appropriate CountDownTimer
     * @return CountDownTimer
     */
    private CountDownTimer creatCountDownTimer() {

        MainActivity.getPeriodTV().setText(getPeriodName());
        return new CountDownTimer(getTime() * 60000, 1000) {
            @Override
            public void onTick(long l) {
                DecimalFormat myFormatter = new DecimalFormat("00");
                timeToSave = l;
                int seconds = (int) l / 1000 % 60;
                int minutes = (int) l / 60000;
                String time = myFormatter.format(minutes) + ":" + myFormatter.format(seconds);
                MainActivity.getTimerTV().setText(time);
                Notification.Builder mBuilder = MainActivity.getmBuilder();
                if (mBuilder != null) {
                    mBuilder.setContentTitle(getPeriodName());
                    mBuilder.setContentText(time);
                    MainActivity.getNm().notify(1, mBuilder.build());
                }
            }

            @Override
            public void onFinish() {
                MediaPlayer mp = MediaPlayer.create(MainActivity.getmContext(), R.raw.whistle);
                mp.start();
                period++;
                Log.wtf("Timer", period + "");
                timer = creatCountDownTimer();
                timer.start();
            }
        };


    }

    /**
     * Returns name of the period by its number
     * @return name of the period
     */
    private String getPeriodName() {
        switch (period) {
            case 0:
                return "First period";
            case 1:
                return "First break";
            case 2:
                return "Second period";
            case 3:
                return "Second (Big) break";
            case 4:
                return "Third period";
            case 5:
                return "Third break";
            case 6:
                return "Fourth period";
            case 7:
                return "Fourth break";
            default:
                return (period / 2 + 1) + (period % 2 == 0 ? "th period" : "th break");
        }
    }

    /**
     * Is timer variable null
     * @return true is timer variabe isn't null
     */
    public boolean isCreated() {
        return timer != null;
    }

    /**
     * Pause CountDownTimer
     */
    public void pause() {
        timer.cancel();
    }

    /**
     * Resume CountDownTimer
     */
    public void resume() {
        timer = new CountDownTimer(timeToSave, 1000) {
            @Override
            public void onTick(long l) {
                DecimalFormat myFormatter = new DecimalFormat("00");
                timeToSave = l;
                int seconds = (int) l / 1000 % 60;
                int minutes = (int) l / 60000;
                String time = myFormatter.format(minutes) + ":" + myFormatter.format(seconds);
                MainActivity.getTimerTV().setText(time);
                Notification.Builder mBuilder = MainActivity.getmBuilder();
                if (mBuilder != null) {
                    mBuilder.setContentTitle(getPeriodName());
                    mBuilder.setContentText(time);
                    MainActivity.getNm().notify(1, mBuilder.build());
                }
            }

            @Override
            public void onFinish() {
                period++;
                Log.wtf("Timer", period + "");
                timer = creatCountDownTimer();
                timer.start();
            }
        };
        timer.start();
    }

}
