package ua.study.denysyashchuk.courtcounter;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Creates service for canceling notification, when program is closed from task manager
 */
public class CancelNotifService extends Service {

    public class CancelBinder extends Binder{
        public final Service service;

        public CancelBinder(Service service) {
            this.service = service;
        }
    }

    private NotificationManager nm;
    private final IBinder mBinder = new CancelBinder(this);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.cancelAll();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }
}
